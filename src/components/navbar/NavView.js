import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './NavView.css';
import { faHome, faAddressCard, faSwatchbook, faList, faFileDownload, faArrowCircleRight, faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons'


export default class NavView extends Component {

    state = {
        navOptions: [
            {
                name: "Home",
                icon: faHome
            },
            {
                name: "Activity",
                icon: faList
            },
            {
                name: "Project",
                icon: faSwatchbook
            },
            {
                name: "Resume",
                icon: faFileDownload
            },
            {
                name: "About",
                icon: faAddressCard
            }
        ],
        isNavOpen : false
    }


    render() {
        const { onNavClick } = this.props;
        return (
            <div className="Nav-body" style={{width: this.state.isNavOpen ? "100px" : "10px"}}>
                <div className="Nav-menu-container" style={{visibility: this.state.isNavOpen ? "visible" : "hidden"}}>
                    <center>
                        <img className="Nav-image" src={require('../../assests/images/image.jpg')} alt="Yubraj Oli" />
                    </center>
                    <div className="Nav-opt-container">
                        {
                            this.state.navOptions.map((item, index) => {
                                return <div className="Nav-option" key={index} onClick={() => onNavClick(item.name)}>
                                    <FontAwesomeIcon icon={item.icon} color="#1F2224" size={"lg"} />
                                </div>
                            })
                        }
                    </div>
                </div>
                <div className="Nav-drawer-container">
                    <FontAwesomeIcon icon={ this.state.isNavOpen ? faArrowCircleLeft: faArrowCircleRight} color="#1A4A4F" size="lg"  onClick={()=>{this.setState({isNavOpen:!this.state.isNavOpen})}}/>
                </div>
            </div>
        )
    }
}
