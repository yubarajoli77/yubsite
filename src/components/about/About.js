import React from 'react';
import './About.css';
import { myDetail, socialLinks } from '../../constants/constants';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressBook, faPhone, faMailBulk } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faTwitter, faInstagram, faLinkedinIn, faGithub } from '@fortawesome/free-brands-svg-icons';

function About() {
    const openSocialPage = url => {
        url ? window.open(url, "_blank") : alert("Sorry, currently unavailable.");
    }

    return (
        <div className="About-body">
            <div className="About-detail-text">
                <strong>{myDetail}</strong>
            </div>
            <div className="About-contact-container">
                <div className="About-contact-item About-location">
                    <div className="About-contact-icon">
                        <FontAwesomeIcon icon={faAddressBook} size="lg" />
                    </div>
                    <strong>Visit Me</strong>
                    <span>Janta Marga, Maitidevi</span>
                    <span>Kathmandu Nepal</span>
                </div>
                <div className="About-contact-item About-phone">
                    <div className="About-contact-icon">
                        <FontAwesomeIcon icon={faPhone} size="lg" />
                    </div>
                    <strong>Call Me</strong>
                    <span>You can give me ring</span>
                    <span>+977-9849774795</span>

                </div>
                <div className="About-contact-item About-message">
                    <div className="About-contact-icon">
                        <FontAwesomeIcon icon={faMailBulk} size="lg" />
                    </div>
                    <strong>Message Me</strong>
                    <span>You can also connect me via email</span>
                    <span>yubarajoli77@gmail.com</span>

                </div>
            </div>
            <div className="About-follow-container">
                <h3>Find Me</h3>
                <div className="About-social">
                    <div className="About-social-icon" >
                        <FontAwesomeIcon icon={faGithub} size="lg" onClick={() => { openSocialPage(socialLinks.github) }} />
                    </div>
                    <div className="About-social-icon" >
                        <FontAwesomeIcon icon={faLinkedinIn} size="lg" onClick={() => { openSocialPage(socialLinks.linkedIn) }} />
                    </div>
                    <div className="About-social-icon" >
                        <FontAwesomeIcon icon={faTwitter} size="lg" onClick={() => { openSocialPage(socialLinks.twitter) }} />
                    </div>
                    <div className="About-social-icon" >
                        <FontAwesomeIcon icon={faFacebook} size="lg" onClick={() => { openSocialPage(socialLinks.facebook) }} />
                    </div>
                    <div className="About-social-icon" >
                        <FontAwesomeIcon icon={faInstagram} size="lg" onClick={() => { openSocialPage(socialLinks.instagram) }} />
                    </div>
                </div>
            </div>

        </div>
    )
}

export default About
