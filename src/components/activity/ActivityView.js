import React, { Component } from 'react';
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { faSchool, faTasks, faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { timeLineIcon, timelineActivities } from '../../constants/constants';
export default class ActivityView extends Component {

    getIcon = icon => {
        let faIcon;
        if (icon === timeLineIcon.ICON_WORK)
            faIcon = faTasks;
        else if (icon === timeLineIcon.ICON_EDUCATION)
            faIcon = faSchool
        else
            faIcon = faStar;

        return <FontAwesomeIcon icon={faIcon} />
    }

    render() {

        return (
            <div className="Acitvity-body">
                <VerticalTimeline>
                    {
                        timelineActivities.map((activity, index) => {
                            return <VerticalTimelineElement
                                className={activity.className}
                                contentStyle={activity.contentStyle}
                                contentArrowStyle={activity.contentArrowStyle}
                                date={activity.date}
                                iconStyle={activity.iconStyle}
                                icon={this.getIcon(activity.icon)}
                                key = {index}
                            >
                                <h3 className="vertical-timeline-element-title">{activity.title}</h3>
                                <h4 className="vertical-timeline-element-subtitle">{activity.subtitle}</h4>
                                <p>{activity.detail}</p>
                            </VerticalTimelineElement>
                        })
                    }
                    <VerticalTimelineElement
                        iconStyle={{ background: 'rgb(16, 204, 82)', color: '#fff' }}
                        icon={<FontAwesomeIcon icon={faStar} />}
                    />
                </VerticalTimeline>
            </div >
        )
    }
}
