import React from 'react'
import './Project.css';
import { doneProjects } from '../../constants/constants';

function Project() {
    return (
        <div className="Project-body container">
            {
                doneProjects.map((item, index) => {
                    return <div className="Project-card-container" key={index}>
                        <img className="Project-card-image" src={item.icon} alt="Project Icon" />
                        <h4>{item.title}</h4>
                        <span className="Project-card-detail">{item.detail}</span>
                    </div>
                })
            }
        </div>
    )
}

export default Project
