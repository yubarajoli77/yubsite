import React from 'react';
import './Service.css';
import { services } from '../../constants/constants';

function Service() {
    return (
        <div className="Service-body">
            {
                services.map((service, index) => {
                    return <div className="Service-card-container" key={index}>
                        <img className="Service-card-image" src={service.icon} alt="Service Icon" />
                        <h3>{service.title}</h3>
                    </div>
                })
            }
        </div>
    )
}

export default Service
