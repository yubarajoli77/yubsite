import React, { Component } from 'react';
import './HomeView.css';

export default class HomeView extends Component {
    render() {
        return (
            <div className="Home-body">
                <div className="Home-image-container">
                    <img className="Home-image" src={require("../../assests/images/image.jpg")} alt="Yubraj Oli" />
                    <strong className="Home-intro-text">Cool guy with 3+ years of work experience on Mobile application development. Want know more about me??? Keep Scrolling or you can directly contact me.</strong>
                </div>
            </div>
        )
    }
}
