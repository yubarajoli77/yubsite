import React from 'react';
import './BigButton.css';

function BigButton({buttonText, onClick}) {
    return (
        <div>
            <div className="Big-button" onClick={()=>onClick("About")}>{buttonText}</div>
            {/* <a href={hrefString} className="button">{buttonText}</a> */}
        </div>
    )
}

export default BigButton
