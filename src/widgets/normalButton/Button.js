import React from 'react';
import './Button.css'

function Button({btnText, onClick}) {
    return (
    <div className="Button-normal-button" onClick={()=>onClick("About")}>{btnText}</div>
    )
}

export default Button