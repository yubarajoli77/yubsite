export const timeLineIcon = {
    ICON_WORK : "Work",
    ICON_EDUCATION : "Education",
    ICON_STAR : "Star"
}

const timeLineClassNames = {
    ELEMENT_WORK : "vertical-timeline-element--work",
    ELEMENT_EDUCATION : "vertical-timeline-element--education",
    ELEMENT_TITLE : "vertical-timeline-element-title",
    ELEMENT_SUBTITLE : "vertical-timeline-element-subtitle",
}

export const timelineActivities = [
    {
        className : timeLineClassNames.ELEMENT_WORK,
        contentStyle : {background: 'rgb(33, 150, 243)', color: '#fff' },
        contentArrowStyle : { borderRight: '7px solid  rgb(33, 150, 243)' },
        date : "2011 - present",
        dateClassName : "Activity-date",
        iconStyle :{ background: 'rgb(33, 150, 243)', color: '#fff' },
        icon : timeLineIcon.ICON_WORK,
        titleClassName : timeLineClassNames.ELEMENT_TITLE,
        subTitleClassName : timeLineClassNames.ELEMENT_SUBTITLE,
        title: "Mobile Application Developer",
        subtitle: "Amnil Technology Pvt. Ltd. ",
        detail: "Work as an Mobile Application Developer from 2019-02-01 to present."
    },{
        className : timeLineClassNames.ELEMENT_WORK,
        contentStyle : {background: 'rgb(233, 30, 99)', color: '#fff' },
        contentArrowStyle : { borderRight: '7px solid  rgb(233, 30, 99)' },
        date : "2011 - present",
        dateClassName : "Activity-date",
        iconStyle :{ background: 'rgb(233, 30, 99)', color: '#fff' },
        icon : timeLineIcon.ICON_WORK,
        titleClassName : timeLineClassNames.ELEMENT_TITLE,
        subTitleClassName : timeLineClassNames.ELEMENT_SUBTITLE,
        title: "Android Developer",
        subtitle: "Softech Foundation Pvt. Ltd. ",
        detail: "Work as an Android Application Developer from 2018-02-01 to 2019-01-30"

    },{
        className : timeLineClassNames.ELEMENT_WORK,
        contentStyle : {background: 'rgb(33, 150, 243)', color: '#fff' },
        contentArrowStyle : { borderRight: '7px solid  rgb(33, 150, 243)' },
        date : "2011 - present",
        dateClassName : "Activity-date",
        iconStyle :{ background: 'rgb(33, 150, 243)', color: '#fff' },
        icon : timeLineIcon.ICON_WORK,
        titleClassName : timeLineClassNames.ELEMENT_TITLE,
        subTitleClassName : timeLineClassNames.ELEMENT_SUBTITLE,
        title: "Android Developer",
        subtitle: "Texas Imaginology Pvt. Ltd. ",
        detail: "Work as an Android Application Developer from 2017-02-01 to 2019-05-30. After joining Softech I keep doing support for almost 6-7 months."
    },{
        className : timeLineClassNames.ELEMENT_EDUCATION,
        contentStyle : {background: 'rgb(233, 30, 99)', color: '#fff' },
        contentArrowStyle : { borderRight: '7px solid  rgb(233, 30, 99)' },
        date : "2011 - present",
        dateClassName : "Activity-date",
        iconStyle :{ background: 'rgb(233, 30, 99)', color: '#fff' },
        icon : timeLineIcon.ICON_EDUCATION,
        titleClassName : timeLineClassNames.ELEMENT_TITLE,
        subTitleClassName : timeLineClassNames.ELEMENT_SUBTITLE,
        title: "Android Developer",
        subtitle: "Amnil Technology Pvt. Ltd. "
    },{
        className : timeLineClassNames.ELEMENT_EDUCATION,
        contentStyle : {background: 'rgb(33, 150, 243)', color: '#fff' },
        contentArrowStyle : { borderRight: '7px solid  rgb(33, 150, 243)' },
        date : "2011 - present",
        dateClassName : "Activity-date",
        iconStyle :{ background: 'rgb(33, 150, 243)', color: '#fff' },
        icon : timeLineIcon.ICON_EDUCATION,
        titleClassName : timeLineClassNames.ELEMENT_TITLE,
        subTitleClassName : timeLineClassNames.ELEMENT_SUBTITLE,
        title: "Android Developer",
        subtitle: "Amnil Technology Pvt. Ltd. "
    },{
        className : timeLineClassNames.ELEMENT_EDUCATION,
        contentStyle : {background: 'rgb(233, 30, 99)', color: '#fff' },
        contentArrowStyle : { borderRight: '7px solid  rgb(233, 30, 99)' },
        date : "2011 - present",
        dateClassName : "Activity-date",
        iconStyle :{ background: 'rgb(233, 30, 99)', color: '#fff' },
        icon : timeLineIcon.ICON_EDUCATION,
        titleClassName : timeLineClassNames.ELEMENT_TITLE,
        subTitleClassName : timeLineClassNames.ELEMENT_SUBTITLE,
        title: "Android Developer",
        subtitle: "Amnil Technology Pvt. Ltd. "
    },
]

export const doneProjects = [
    {
        icon: require('../assests/images/image.jpg'),
        title: "QFX Cinemas",
        detail: "QFX Cinema is a mobile application which allow you to find out details about currently running and upcomming cinemas along with book and buy ticket online."
    },{
        icon: require('../assests/images/image.jpg'),
        title: "QFX Cinemas",
        detail: "QFX Cinema is a mobile application which allow you to find out details about currently running and upcomming cinemas along with book and buy ticket online."
    },{
        icon: require('../assests/images/image.jpg'),
        title: "QFX Cinemas",
        detail: "QFX Cinema is a mobile application which allow you to find out details about currently running and upcomming cinemas along with book and buy ticket online."
    },{
        icon: require('../assests/images/image.jpg'),
        title: "QFX Cinemas",
        detail: "QFX Cinema is a mobile application which allow you to find out details about currently running and upcomming cinemas along with book and buy ticket online."
    },{
        icon: require('../assests/images/image.jpg'),
        title: "QFX Cinemas",
        detail: "QFX Cinema is a mobile application which allow you to find out details about currently running and upcomming cinemas along with book and buy ticket online."
    },{
        icon: require('../assests/images/image.jpg'),
        title: "QFX Cinemas",
        detail: "QFX Cinema is a mobile application which allow you to find out details about currently running and upcomming cinemas along with book and buy ticket online."
    },{
        icon: require('../assests/images/image.jpg'),
        title: "QFX Cinemas",
        detail: "QFX Cinema is a mobile application which allow you to find out details about currently running and upcomming cinemas along with book and buy ticket online."
    },
]

export const services = [
    {
        icon: require('../assests/images/image.jpg'),
        title: "Android App Development",
    },{
        icon: require('../assests/images/image.jpg'),
        title: "React Native App Development",
    },
    {
        icon: require('../assests/images/image.jpg'),
        title: "React JS",
    },
    {
        icon: require('../assests/images/image.jpg'),
        title: "MERN STACK",
    },
]

export const myDetail = "I am Mobile Application Developer, continously working on this field from 2017. I have worked on both native as well as cross platform mobile application projects. You can find the details about my projects on my resume, which you can easily download it from the navbar. You can always contact me via email or call.";
 
export const socialLinks = {
    facebook: "https://www.facebook.com/yubraj.oli",
    instagram: "https://www.instagram.com/yubarajoli77/",
    twitter: "https://twitter.com/mathew_uv",
    linkedIn: "https://www.linkedin.com/in/yuba-raj-oli-09b223144/",
    github: "https://github.com/yubarajoli77",
    resumeDownloadUrl: "https://drive.google.com/file/d/1QKxSZxFDYx7v6S5rTkhlfc_fMZTBzTe1/view?usp=sharing"
}