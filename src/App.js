import React, { useRef } from 'react';
import './App.css';
import NavView from './components/navbar/NavView';
import HomeView from './components/home/HomeView';
import ActivityView from './components/activity/ActivityView';
import BigButton from './widgets/bigButton/BigButton';
import Project from './components/work/Project';
import Service from './components/service/Service';
import Button from './widgets/normalButton/Button';
import About from './components/about/About';
import {socialLinks} from './constants/constants';

function App() {
    const homeRef = useRef();
    const activityRef = useRef();
    const projectRef = useRef();
    const serviceRef = useRef();
    const aboutRef = useRef();

    const onNavClick = tag => {
        if (tag === "Home")
            scrollToSection(homeRef);
        else if (tag === "Activity")
            scrollToSection(activityRef);
        else if (tag === "Project")
            scrollToSection(projectRef);
        else if (tag === "About")
            scrollToSection(aboutRef);
        else if (tag === "Resume")
            window.open(socialLinks.resumeDownloadUrl, "_blank");
        else
            scrollToSection(homeRef)

        // var tempItem = document.getElementById(tag);
        // const item = tempItem ? tempItem : document.getElementById("Home");
        // var diff = (item.offsetTop - window.scrollY) / 20;
        // if (!window._lastDiff) {
        //     window._lastDiff = 0;
        // }

        // if (Math.abs(diff) > 2) {
        //     window.scrollTo(0, (window.scrollY + diff));
        //     clearTimeout(window._TO);

        //     if (diff !== window._lastDiff) {
        //         window._lastDiff = diff;
        //         window._TO = setTimeout(scrollToItem, 15, item);
        //     }
        // } else {
        //     console.timeEnd('test');
        //     window.scrollTo(0, item.offsetTop);
        // }


    }

    const scrollToSection = ref => {
        if (ref)
            window.scrollTo(0, ref.current.offsetTop)
    }

    // const scrollToItem = item => {
    //     var diff = (item.offsetTop - window.scrollY) / 8
    //     if (Math.abs(diff) > 1) {
    //         window.scrollTo(0, (window.scrollY + diff))
    //         clearTimeout(window._TO)
    //         window._TO = setTimeout(scrollToItem, 30, item)
    //     } else {
    //         window.scrollTo(0, item.offsetTop)
    //     }
    // }

    return (
        <div className="App">
            <NavView onNavClick={onNavClick} />
            <div className="App-home" id="Home" ref={homeRef}>
                <HomeView />
            </div>
            <div className="App-body" >
                <div className="App-section" id="Activity" ref={activityRef}>
                    <h2>My Activity Timeline</h2>
                    <p>You can be part of my time line if you want.</p>
                    <BigButton buttonText={"Hire Me!!!"} onClick={onNavClick} />
                    <ActivityView />
                </div>
                <div className="App-section" id="Project" ref={projectRef}>
                    <h2>What I Have Done Till Now?</h2>
                    <Project />
                    <br />
                    <strong>Love my work ??? You can contact me or leave a message.</strong>
                    <Button btnText="Contact Me" onClick={onNavClick} />
                </div>
                <div className="App-section" id="Service" ref={serviceRef}>
                    <h2>What I Offer?</h2>
                    <Service />
                    <br />
                    <strong> Want to hire me??? You can contact me or leave a message.</strong>
                    <Button btnText="Contact Me" onClick={onNavClick} />

                </div>
                <div className="App-section" id="About" ref={aboutRef}>
                    <h2>About Me</h2>
                    <About />
                </div>
            </div>
        </div>
    );
}

export default App;